<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('home',[
            'projects' => Post::where('type', 'project')->get()
        ]);
    }

    public function project(Post $post)
    {
        return view('project',['post' => $post]);
    }
}
