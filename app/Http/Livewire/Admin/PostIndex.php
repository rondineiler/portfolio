<?php

namespace App\Http\Livewire\Admin;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class PostIndex extends Component
{
    use WithPagination;

    public $search;

    protected $listeners = ['$refresh'];

    public function mount()
    {
        $this->dispatchBrowserEvent('notify',['success' => 'Post cadastrado com sucesso!']);
    }

    public function render()
    {
        return view('livewire.admin.post-index',[
            'posts' => Post::where('title','like','%'.$this->search.'%')->orderBy('order','DESC')->paginate(10)
        ])->layout('layouts.app',['header'=>'Posts']);
    }
}
