<?php

namespace App\Http\Livewire\Admin;

use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class PostCrud extends Component
{

    use WithFileUploads;

    public $current_id, $mode;
    public $title, $description, $type, $slug, $order, $cover, $coverInput;

    protected $listeners = ['destroy','edit'];

    protected $rules = [
        'title' => 'required|min:5',
        'type' => 'required',
    ];

    public function mount()
    {
        $this->description = '';
    }

    public function resetInputs()
    {
        $this->reset();
        $this->description = '';
        $this->resetErrorBag();
        $this->dispatchBrowserEvent('reset');
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $this->mode = 'edit';

        $this->current_id = $id;

        $post = Post::findOrFail($id);

        $this->fill($post);

        $this->dispatchBrowserEvent('open-modal',['name' => 'new']);
    }

    public function store()
    {

        $this->validate();

        if($this->current_id && $this->mode === 'edit')
        {
            $post = Post::findOrFail($this->current_id);
        }
        else
        {
            $post = new Post();
            $post->order = Post::max('order') + 1;
        }

        $post->title = $this->title;
        $post->description = $this->description;
        $post->type = $this->type;

        if($this->coverInput)
        {
            $this->validate([
                'coverInput' => 'nullable|image|max:1024',
            ]);

            if($post->cover){
                Storage::delete('post/covers/'.$post->cover);
            }
            if($path = $this->coverInput->store('post/covers')){
                $path = explode('post/covers/',$path);
                $post->cover = $path[1];
            }
        }
        $post->save();

        $this->dispatchBrowserEvent('notify',['success' => 'Post cadastrado com sucesso!']);
        $this->dispatchBrowserEvent('close-modal',['time' => 0]);
        $this->resetInputs();
        $this->emitTo('admin.post-index','$refresh');
    }

    public function destroy($id)
    {
        if($id)
        {
            $post = Post::find($id);

            if($post->cover){
                Storage::delete('post/covers/'.$post->cover);
            }

            if($post->delete())
            {
                $this->dispatchBrowserEvent('notify',['success' => 'Post excluído com Sucesso!!']);
                $this->emitTo('admin.post-index','$refresh');
            }
        }
    }

    public function render()
    {
        return view('livewire.admin.post-crud');
    }
}
