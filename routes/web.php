<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\SiteController;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\PostIndex;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[SiteController::class,'index'])->name('home');
Route::get('/testes',function(){
    echo 'Teste is running';
});
Route::get('portfolio/{post:slug}',[SiteController::class,'project'])->name('project');

Route::prefix('admin')->middleware(['auth'])->name('admin.')->group(function () {
    Route::get('/',Dashboard::class)->name('dashboard');
    Route::get('/posts',PostIndex::class)->name('posts');
});

require __DIR__.'/auth.php';
