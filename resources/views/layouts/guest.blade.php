<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rondineier de Paula</title>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <link rel="stylesheet" href="{{url(mix('css/app.css'))}}">
    <link rel="stylesheet" href="{{url(mix('css/text-editor.css'))}}">
    <!-- Scripts -->
    <script src="{{ url(mix('js/app.js')) }}" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body x-data="{scroll: 0}" x-on:scroll.window="scroll = window.pageYOffset" class="min-h-screen bg-no-repeat bg-cover bg-center bg-fixed" style="background-image: url('{{asset('storage/images/bg.jpg')}}')">
    <header x-data="{menu: false}" class="w-full">
        <div
        x-bind:class="scroll > 100 ? 'bg-gray-900 md:bg-opacity-70' : ''"
        class="flex flex-col md:flex-row text-white items-center justify-center py-5 w-full fixed top-0 left-0 z-40">
            <button class="md:hidden" @click="menu = !menu">
                <svg x-show="menu === false" xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 transition-opacity" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
                <svg x-show="menu === true" xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 transition-opacity" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
            <x-menu.group-menu>
                <x-menu.item-menu :href="route('home')" text="Início"/>
                <x-menu.item-menu href="#projetos" text="Projetos"/>
                <x-menu.item-menu href="#contatos" text="Contato"/>
            </x-menu.group-menu>
        </div>
    </header>
    <main class="min-h-screen">
        {{$slot}}
    </main>
    <footer>
    </footer>
</body>

</html>

