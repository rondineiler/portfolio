<div class="w-full p-5">
    <x-admin.searchbox.searchbox
    :new="[
        'title'=>'Novo Post'
    ]"/>
    <div class="w-full">
        @foreach ($posts as $post)
            <div class="p-2 flex justify-between bg-white mt-2 border border-gray-300 rounded-lg items-center">
                <h2 class="text-md">{{$post->title}}</h2>
                <div class="flex">
                    <x-buttons.btn class="bg-yellow-400 text-black mr-2" x-on:click="$wire.emit('edit',{{$post->id}})">
                        Editar
                    </x-buttons.btn>
                    <x-buttons.btn class="bg-red-500" x-on:click="confirm('Tem certeza que deseja excluir?') ? $wire.emit('destroy', {{$post->id}}) : ''">
                        Deletar
                    </x-buttons.btn>
                </div>
            </div>
        @endforeach
    </div>
</div>
@livewire('admin.post-crud')
