<div>
    <x-modal.modal title="Post" nameModal="new">
        <x-form.group id="title" label="Título" type="text"/>
        <x-form.group id="coverInput" label="Imagem Capa" type="file" icon="image" placeholder="Selecione uma imagem de capa"/>
        <x-form.group id="description" label="Descrição" type="text-editor"/>
        <x-form.group id="type" label="Tipo" type="select">
            <option value="">Selecione um tipo</option>
            <option value="project">Projeto</option>
            <option value="post">Post</option>
        </x-form.group>
        <x-buttons.md class="bg-green-500 m-4 text-white" wire:click="store()">
            Salvar
        </x-buttons.md>
    </x-modal.modal>
</div>

