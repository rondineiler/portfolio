<x-guest-layout>
    <section class="relative w-full flex flex-col items-center justify-center min-h-screen">
        <figure class="z-10 rounded-full border-[20px] border-opacity-60 border-gray-600 w-64 h-64 shadow-xl">
            <img src="{{asset('storage/images/rondi.jpeg')}}" alt=""
                class="w-full h-full rounded-full border-[10px] border-green-800 shadow-lg">
        </figure>
        <div class="rounded-lg pb-10 -mt-1 px-14">
            <h1 class="text-white font-light text-[65px] text-center">
                RONDINEILER
            </h1>
            <div class="-mt-3 ">
                <p class="text-white text-[30px] text-center font-light">Web Developer</p>
            </div>
        </div>
        <article class="text-white text-center mt-4 text-lg mx-auto max-w-[700px]">
            <p>
                Olá! Meu nome é Rondineiler de Paula, sou desenvolvedor fullstack e atuo com foco em
                soluções para a
                web, sou graduado em Sistemas de Informação e atuo na área a 3 anos desenvolvendo sites, landing
                pages e sistemas personalizados com as melhores práticas.
            </p>
        </article>
        <svg xmlns="http://www.w3.org/2000/svg" class="absolute bottom-2 animate-bounce h-10 w-10 text-white"
            fill="current" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
        </svg>
    </section>
    <section>
        <section class="pt-10 text-white max-w-[1150px] mx-auto text-center">
            <h2 class="text-[40px]" id="projetos">
                Projetos
            </h2>
            <div class="flex flex-wrap pt-4 space-x-[5px] space-y-[5px]">
                @foreach ($projects as $project)
                    <x-cards.project :project="$project"/>
                @endforeach
            </div>
        </section>
    </section>
    <section>
        <section class="pt-16 text-white max-w-[1150px] mx-auto">
            <header class="text-center flex flex-col items-center">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-10 w-10" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
                </svg>
                <h2 class="text-[40px]" id="contatos">
                    Contatos
                </h2>
            </header>
            <div class="flex flex-col items-center justify-center pt-10 pb-20 space-y-5">
                <a href="https://api.whatsapp.com/send?phone=5524988671720" target="_blank"
                    class="flex items-center hover:scale-105 transition">
                    <div class="icons-contact">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-900 h-[55%] w-[55%]"
                            viewBox="0 0 24 24">
                            <path
                                d="M.057 24l1.687-6.163c-1.041-1.804-1.588-3.849-1.587-5.946.003-6.556 5.338-11.891 11.893-11.891 3.181.001 6.167 1.24 8.413 3.488 2.245 2.248 3.481 5.236 3.48 8.414-.003 6.557-5.338 11.892-11.893 11.892-1.99-.001-3.951-.5-5.688-1.448l-6.305 1.654zm6.597-3.807c1.676.995 3.276 1.591 5.392 1.592 5.448 0 9.886-4.434 9.889-9.885.002-5.462-4.415-9.89-9.881-9.892-5.452 0-9.887 4.434-9.889 9.884-.001 2.225.651 3.891 1.746 5.634l-.999 3.648 3.742-.981zm11.387-5.464c-.074-.124-.272-.198-.57-.347-.297-.149-1.758-.868-2.031-.967-.272-.099-.47-.149-.669.149-.198.297-.768.967-.941 1.165-.173.198-.347.223-.644.074-.297-.149-1.255-.462-2.39-1.475-.883-.788-1.48-1.761-1.653-2.059-.173-.297-.018-.458.13-.606.134-.133.297-.347.446-.521.151-.172.2-.296.3-.495.099-.198.05-.372-.025-.521-.075-.148-.669-1.611-.916-2.206-.242-.579-.487-.501-.669-.51l-.57-.01c-.198 0-.52.074-.792.372s-1.04 1.016-1.04 2.479 1.065 2.876 1.213 3.074c.149.198 2.095 3.2 5.076 4.487.709.306 1.263.489 1.694.626.712.226 1.36.194 1.872.118.571-.085 1.758-.719 2.006-1.413.248-.695.248-1.29.173-1.414z" />
                        </svg>
                    </div>
                    <p class="ml-3 font-light text-[30px]">
                        (24)98867-1720
                    </p>
                </a>
                <section class="hidden flex items-center">
                    <div class="icons-contact">
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-gray-900 h-[60%] w-[60%]" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207" />
                        </svg>
                    </div>
                </section>
            </div>
        </section>
    </section>
</x-guest-layout>
