<div
    style="display: none; z-index:1000;"
    x-data="{show: false, success: false, error: false, message: false}"
    x-show="show"
    x-transition
    x-on:notify.window="show = true; success = $event.detail.success; error = $event.detail.error; setTimeout(() => {show = false},2500)"
    x-bind:class="{'bg-green-500': success, 'bg-red-500': error}"
    class="fixed
        flex
        items-center top-24
        text-lg right-0 md:right-4
        w-full md:max-w-[350px] p-4 rounded-lg
        border border-green-700
        shadow-2xl
        text-white
        md:bg-opacity-90
">
    <i x-show="success" class="far fa-check-circle text-2xl mr-2"></i>
    <i x-show="error" class="fas fa-exclamation text-lg mr-2"></i>
    <p x-text="success ? success : error"></p>
</div>
