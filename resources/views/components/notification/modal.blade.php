<div
    style="display: none;"
    x-data="{show: false, success: true, error: false, message: false}"
    x-show="show"
    x-transition
    x-on:modal-notify.window="show = true; success = $event.detail.success; error = $event.detail.error; setTimeout(() => {show = false},2500)"
    class="fixed
        flex justify-center items-start
        pt-10 md:py-20
        inset-0
        z-50
        bg-gray-600
        bg-opacity-75
">
    <div class="rounded-xl bg-white py-10 px-6 flex flex-col items-center justify-center w-full max-w-[650px] shadow-xl">
        <i x-show="success" class="far fa-check-circle text-[90px] pb-6 text-green-500"></i>
        <i x-show="error" class="fas fa-exclamation text-[90px] pb-6"></i>
        <p class="text-[35px]" x-text="success ? success : error"></p>
    </div>
</div>
