<label
x-data="{files: null, isUploading: false, progress: 0}"
x-on:livewire-upload-start="isUploading = true"
x-on:livewire-upload-finish="progress = 0, isUploading = false"
x-on:livewire-upload-error="progress = 0, isUploading = false"
x-on:livewire-upload-progress="progress = $event.detail.progress"
x-on:reset.window="files = null, isUploading = false"
class="relative flex inline-file cursor-pointer dark:border-white bg-white bg-opacity-0" for="{{$attributes['id']}}" type="text">
    <input {{$attributes->merge(['class'=>'hidden'])}} x-on:change="files = Object.values($event.target.files)">
    @switch($icon)
        @case('image')
            <svg class="w-6 h-6 text-current-50" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
            </svg>
            @break
        @case('movie')
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-current-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 4v16M17 4v16M3 8h4m10 0h4M3 12h18M3 16h4m10 0h4M4 20h16a1 1 0 001-1V5a1 1 0 00-1-1H4a1 1 0 00-1 1v14a1 1 0 001 1z" />
            </svg>
            @break
        @default
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-current-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
            </svg>
    @endswitch
    <div x-show="isUploading" class="absolute inline-block text-sm text-white bg-blue-500 py-[1px] px-[5px] rounded-full"
    style="top: 50%; left: 50%; transform: translate(-50%,-50%)"><p x-text="progress+'%'"></p></div>
    <div x-show="isUploading" x-transition class="absolute left-2 bottom-0 right-2 rounded-full bg-white h-1">
        <div x-bind:style="{width: progress+'%'}" class="absolute inset-y-0 left-0 rounded-full bg-indigo-500 h-full"></div>
    </div>
    <p class="ml-3" x-text="files ? files.map(file => file.name).join(', ') : '{{$placeholder ?? ''}}'"></p>
</label>
