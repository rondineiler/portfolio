<select {{$attributes->merge(['class'=>'dark:border-white bg-white bg-opacity-0'])}}>
    {{$slot}}
</select>
