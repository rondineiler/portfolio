@props([
    'id',
    'label',
    'type',
    'name',
    'wire'
])
<div {{$attributes->merge(['class'=>'w-full p-1 md:p-3'])}}>
    <x-form.modal.label :for="$id" :label="$label"/>

    @if ($type === 'select')
        <x-form.modal.select class="" :id="$id" :type="$type" wire:model.defer="{{isset($wire) ? $wire.'.'.$id : $id}}">
            {{$slot}}
        </x-form.modal.select>
    @else
        <x-form.modal.input class="" :id="$id" :type="$type" wire:model.defer="{{isset($wire) ? $wire.'.'.$id : $id}}"/>
    @endif

    @error(isset($wire) ? $wire.'.'.$id : $id)
        <x-form.modal.error :message="$message"/>
    @enderror
</div>
