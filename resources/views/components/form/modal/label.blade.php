@props(['label'])
<label {{$attributes->merge(['class'=>'w-full'])}}>{{$label}}</label>
