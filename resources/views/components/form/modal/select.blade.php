<select {{$attributes->merge(['class'=>'bg-white bg-opacity-0'])}}>
    {{$slot}}
</select>
