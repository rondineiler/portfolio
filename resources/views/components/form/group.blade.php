@props([
    'id',
    'label',
    'type',
    'name',
    'wire',
    'group',
    'mode',
    'icon'
])
<div {{$attributes->merge(['class'=>'w-full p-1 md:p-3'])}} x-data>
    <x-form.label for="{{isset($group) ? $group.$id : $id}}" :label="$label"/>
    @switch($type)
        @case('select')
            <x-form.select class="" x-bind:disabled="'{{$mode ?? ''}}' === 'show'" id="{{isset($group) ? $group.$id : $id}}" :type="$type" wire:model.defer="{{isset($wire) ? $wire.'.'.(isset($name) ? $name : $id) : (isset($name) ? $name : $id) }}">
                {{$slot}}
            </x-form.select>
            @break
        @case('text-editor')
            <x-form.text-editor name="{{isset($name) ?? (isset($group) ? $group.$id : $id)}}" x-bind:disabled="'{{$mode ?? ''}}' === 'show'" class="" id="{{isset($group) ? $group.$id : $id}}"/>
            @break
        @case('file')
        <x-form.inline-file :icon="$icon" placeholder="{{$attributes['placeholder'] ?? 'Selecione um arquivo'}}" name="{{isset($name) ?? (isset($group) ? $group.$id : $id)}}" x-bind:disabled="'{{$mode ?? ''}}' === 'show'" class="" id="{{isset($group) ? $group.$id : $id}}" :type="$type" wire:model.defer="{{isset($wire) ? $wire.'.'.(isset($name) ? $name : $id) : (isset($name) ? $name : $id) }}"/>
            @break
        @case('drop-zone')
            <x-form.drop-zone />
            @break
        @default
            <x-form.input placeholder="{{$attributes['placeholder'] ?? ''}}" name="{{isset($name) ?? (isset($group) ? $group.$id : $id)}}" x-bind:disabled="'{{$mode ?? ''}}' === 'show'" class="" id="{{isset($group) ? $group.$id : $id}}" :type="$type" wire:model.defer="{{isset($wire) ? $wire.'.'.(isset($name) ? $name : $id) : (isset($name) ? $name : $id) }}"/>
    @endswitch
    @error(isset($wire) ? $wire.'.'.$id : $id)
        <x-form.error :message="$message"/>
    @enderror
</div>
