@props(['label'])
<label {{$attributes->merge(['class'=>''])}}>{{$label}}</label>
