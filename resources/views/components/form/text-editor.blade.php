<div
    x-data="{
        editorValue: @entangle($id).defer,

        setValue() {
            tinymce.get('{{$id}}').setContent(this.editorValue);
        },
    }"
    x-init="
        tinymce.init({
            selector:'textarea.texteditor',
            height: 300,
            setup: function (editor) {
                editor.on('init', function () {
                    setValue();
                });

                editor.on('blur', function () {
                    editorValue = editor.getContent();
                    contentTextEditor = editor.getContent();
                });

                editor.on('change', function () {
                    contentTextEditor = editor.getContent();
                });

                editor.on('keyup', function () {
                    contentTextEditor = editor.getContent();
                });
            },
            content_css: '{{url(mix('css/text-editor.css'))}}',
            style_formats: [
                {title: 'Project', block: 'h3', classes: 'subtitle-project'},
            ]
        });
        $watch('editorValue', () => setValue())
    "
    wire:ignore
>
    <textarea x-ref="tinymce" {{$attributes->merge(['class' => 'texteditor'])}}></textarea>
</div>
@once
    @push('scripts')
        <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    @endpush
@endonce
