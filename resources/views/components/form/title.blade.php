<div class="w-full py-6">
    <h3 class="text-lg mb-2 px-2">{{$slot}}</h3>
    <hr class="border-t-black border-t-1 dark:border-t-white">
</div>
