@props(['icon','title','nameModal'])
<div wire:ignore.self x-show="modal === '{{$nameModal}}'"
x-on:open-modal.window="modal = $event.detail.name"
x-on:close-modal.window="
    setTimeout(() => {modal = false}, $event.detail.time)
"
class="text-black dark:text-white fixed overflow-y-auto pb-2 pt-3 md:py-6 top-0 left-0 w-full h-full flex items-start justify-center bg-gray-500 bg-opacity-75 z-50">
    <div
    x-show="modal === '{{$nameModal}}'"
    x-transition:enter.duration.500ms
    x-transition:leave.duration.500ms
    class="relative flex-shrink-0 rounded-lg pb-2 shadow-2xl bg-white dark:bg-dark-over w-[98%] max-w-[1100px]">
        <div class="flex items-center justify-between py-2 px-4">
            <h1 class="text-xl">
                @isset($icon)<i class='{{$icon}} mr-3'></i>@endisset{{$title}}
            </h1>
            <button x-on:click="modal = false" class="cursor-pointer">
                <svg
                class="w-6 h-6 text-black dark:text-white"
                fill="true"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                ><path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
                ></path></svg>
            </button>
        </div>
        <hr class="border-t border-gray-100 dark:border-gray-800">
        <div>
            {{$slot}}
            @isset ($footerModal)
            <footer class="w-full">
                <hr class="border-t border-gray-100 dark:border-gray-800">
                {{$footerModal}}
            </footer>
            @endisset
        </div>
    </div>
</div>
<div wire:loading.flex wire:target="show()" style="z-index: 800;" class="fixed flex items-center justify-center fill-current text-ead-third inset-0 bg-ead-first opacity-50"
x-transition.duration.950ms>
    <svg class="animate-spin h-16 w-16" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
        <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
        <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
    </svg>
</div>

