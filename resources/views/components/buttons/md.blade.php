<button {{$attributes->merge(['type'=>'submit','class'=>'flex items-center justify-center rounded shadow-lg transition py-2 px-4 hover:scale-105'])}}>
    <div>
        {{$slot}}
    </div>
</button>
