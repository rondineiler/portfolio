<button {{$attributes->merge(['type'=>'submit','class'=>'p-2 text-black'])}}>
    {{$slot}}
</button>
