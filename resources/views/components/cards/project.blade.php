<a href="{{route('project',['post' => $project->slug])}}" class="rounded-lg relative card-projects group bg-cover bg-center" style="background-image: url('{{$project->photoCover()}}')">
    <div class="rounded-md absolute inset-0 flex items-center justify-center group-hover:bg-gray-900 group-hover:bg-opacity-70">
        <button class="rounded-full border-2 text-blue-500 border-blue-500 px-2 py-1 invisible group-hover:visible transition">
            Ver Mais
        </button>
    </div>
</a>
