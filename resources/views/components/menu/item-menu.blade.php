<li class="w-full md:w-auto">
    <a {{$attributes->merge(['class'=>'w-full block transition px-4 py-2 text-lg hover:text-blue-400'])}}>
        {{$text}}
    </a>
</li>
