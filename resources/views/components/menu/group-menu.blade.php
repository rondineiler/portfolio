<!--Desktop Menu Container-->
<ul
class="hidden md:flex flex-row">
    {{$slot}}
</ul>
<!--Mobile Menu Container-->
<ul
x-transition
x-on:click="menu = !menu"
x-show="menu"
class="flex md:hidden flex-col w-full bg-gray-900 bg-opacity-70 divide-y-[1px] divide-gray-700 divide-solid">
    {{$slot}}
</ul>
