<x-guest-layout>
    <div class="text-white pt-[87px]">
        <section class="bg-gray-500 bg-opacity-30 py-7">
            <div class="mx-auto w-full flex flex-wrap max-w-[1100px]">
                <div class="w-1/2">
                    <img src="{{$post->photoCover()}}" alt="{{$post->title}}" class="rounded-lg shadow-xl object-cover object-center w-full">
                    <section class="w-full text-center">
                        <a href="#" class="
                            inline-block
                            border
                            border-blue-700
                            rounded-sm py-2
                            px-4
                            mt-5
                            shadow-xl
                            transition
                            bg-blue-600
                            hover:bg-blue-500
                            hover:scale-105
                        ">
                            Ver outros projetos
                        </a>
                    </section>
                </div>
                <div class="w-1/2 pl-5 mt-0">
                    <h1 class="text-[35px] font-light">{{$post->title}}</h1>
                    <article class="mt-5">
                        {!!$post->description!!}
                    </article>
                </div>
            </div>
        </section>
    </div>
</x-guest-layout>
